import Vue from 'vue';
import Router from 'vue-router';
import Home from './components/home.vue'
import Login from './components/login.vue'
import MyAccount from './components/myaccount.vue'
import MyCart from './components/mycart.vue'
import Checkout from './components/checkout.vue'
import OrderHistory from './components/orderhistory.vue'
import Categories from './components/categories.vue'
import ProductDetail from './components/productdetail.vue'

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/myaccount',
      component: MyAccount
    },
    {
      path: '/mycart',
      component: MyCart
    },
    {
      path: '/checkout',
      component: Checkout
    },
    {
      path: '/orderhistory',
      component: OrderHistory
    },
    {
      path: '/subcategory/:id',
      component: Categories
    },
    {
      path: '/productdetail/:id/:supplier',
      component: ProductDetail
    }
  ]
});