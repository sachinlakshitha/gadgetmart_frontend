import axios from 'axios';
import constant from '../util/constant';

const API_URL = constant.API_URL+'api/category';

class CategoryService {
    getCategories() {
        return axios.get(API_URL);
    }
}

export default new CategoryService();