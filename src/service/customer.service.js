import axios from 'axios';
import constant from '../util/constant';

const API_URL = constant.API_URL+'api/customer/';

class CustomerService {
    register(customer) {
        return axios.post(API_URL + 'signup', customer);
    }

    login(customer) {
        return axios
          .post(API_URL + 'token', customer)
          .then(response => {    
                return response.data;
          });
    }

    update(customer) {
        return axios.put(API_URL + 'update', customer);
    }

}

export default new CustomerService();