import axios from 'axios';
import constant from '../util/constant';

const API_URL = constant.API_URL+'api/subcategory';

class SubCategoryService {
    getProducts(name) {
        return axios.get(API_URL+"/"+name);
    }
}

export default new SubCategoryService();