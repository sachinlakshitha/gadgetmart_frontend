import axios from 'axios';
import constant from '../util/constant';

const API_URL = constant.API_URL+'api/order';

class OrderService {
    saveOrder(order) {
        return axios.post(API_URL + '', order);
    }

    getOrders(customerid) {
        return axios.get(API_URL + "/" + customerid);
    }
}

export default new OrderService();