import axios from 'axios';
import constant from '../util/constant';

const API_URL = constant.API_URL+'api/';

class ProductService {
    getProductDetail(id,supplier) {
        return axios.get(API_URL+'product_detail/'+id+'/'+supplier);
    }

    getProductByCategory() {
        return axios.get(API_URL+'product/category');
    }
}

export default new ProductService();